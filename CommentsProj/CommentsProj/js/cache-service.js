﻿var myGlobal = myGlobal || {};
myGlobal.cacheService = {};

(function () {
    var key = "site_username";

    function getVal() {
        return window.localStorage.getItem(key);
    }

    function setVal(obj) {
        if (obj.length>0)
            window.localStorage.setItem(key, obj);
    }

    myGlobal.cacheService = {
        save: setVal,
        read: getVal
    };
})();