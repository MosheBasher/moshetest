﻿var myGlobal = myGlobal || {};
myGlobal.userService = {};

(function () {
    function saveUser(name) {
        if (userIsValid(name)) {
            myGlobal.cacheService.save(name);
            return true;
        }
        return false;
    }

    function getCurrentUser() {
        return myGlobal.cacheService.read();
    }

    function userIsValid(name) {
        if (name.length > 0)
            return true;
        return false;
    }

    myGlobal.userService = {
        save: saveUser,
        getCurrent: getCurrentUser
    }
})();