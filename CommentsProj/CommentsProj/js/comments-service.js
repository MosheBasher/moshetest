﻿var myGlobal = myGlobal || {};
myGlobal.commentsService = {};

(function () {
    var myCommentSection = "comments-section";

    function addComment(text) {
        var commentSection = getCommentsSection();
        var user = myGlobal.userService.getCurrent();

        if (!commentSection)
            return;
        commentSection.append(user + ": ");
        var newComment = myGlobal.generateComment.createComment(text);
        commentSection.append(newComment);
    }

    function getCommentsSection() {
        var commentSection = document.getElementsByClassName(myCommentSection);

        if (commentSection.length === 1)
            return commentSection[0];

        return null;
    }

    myGlobal.commentsService = {
        addComments: addComment
    };
})();