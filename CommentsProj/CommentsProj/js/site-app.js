﻿var myGlobal = myGlobal || {};

(function siteStartUp() {

	// No user stored in cache - first login
	if (myGlobal.userService.getCurrent() == null) {
		// Show login menu
		var element = document.getElementsByClassName("js-user-name-insert");
		for (var i = 0; i < element.length; i++) {
			element[i].classList.add("disp-block");
			element[i].classList.remove('disp-none');
		}
	}
		// Welcome menu
	else {
		document.getElementById("WelcomeText").innerHTML +=
        " " + myGlobal.userService.getCurrent();

		// Show welcome menu
		var element = document.getElementsByClassName("welcome-message-hide");
		for (var i = 0; i < element.length; i++) {
			element[i].classList.add("welcome-message-show");
			element[i].classList.remove('welcome-message-hide');
		}
	}
})();

myGlobal.showMenu = function () {
	var input = document.getElementById("Username").value;
	if (myGlobal.userService.save(input)) {
		// Show page content
		var element = document.getElementsByClassName("page-content-hide");
		for (var i = 0; i < element.length; i++) {
			element[i].classList.add("page-content-show");
			element[i].classList.remove('page-content-hide');
		}
	}
}

myGlobal.sendComment = function () {
	var input = document.getElementById("CommentText").value;
	myGlobal.commentsService.addComments(input);
}