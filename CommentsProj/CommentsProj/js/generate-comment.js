﻿var myGlobal = myGlobal || {};
myGlobal.generateComment = {};

(function() {
    function createComment(text) {
        var newDiv = document.createElement("div");
        var newArticle = document.createElement("article");

        newArticle.innerHTML = text;
        newArticle.style.color = "red";

        newDiv.append(newArticle);
        return newDiv;
    }

    myGlobal.generateComment = {
        createComment: createComment
    };
})();