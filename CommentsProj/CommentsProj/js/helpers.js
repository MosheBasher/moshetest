﻿var myGlobal = myGlobal || {};
myGlobal.DOMHelpers = {};

(function() {
	function toggleClass(element, className) {
		if (element.classList.contains(className))
			element.classList.remove(className);
		else {
			element.classList.add(className);
		}
	}

	myGlobal.DOMHelpers = {
		toggleClass: toggleClass
	};
})();

// TODO:
// refactor all js
// more helper func - if only one element exists.
// css+js to public folder
// move all project to new proj with web api
// create xml database
// create xml-adapter calsses : one to handle the file, one to control comments 
// add \ get comments with postman (get and post..)
// refactor all js to jquery